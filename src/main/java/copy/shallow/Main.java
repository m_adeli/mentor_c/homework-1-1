package copy.shallow;

/**
 * @author yourname
 */
public class Main {

    public static void main(String[] args) {

        Book myBook = new Book("War & Peace", "Novel", "Leo Tolstoy");
        Course myCourse = new Course("Literature");
        Teacher t1 = new Teacher("Ali", myCourse, myBook);
        Teacher t2 = null;

        try {
            t2 = t1.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        System.out.println(t1.getCourse().getSubject());

        t2.getCourse().setSubject("Science");

        System.out.println(t1.getCourse().getSubject());
    }
}
