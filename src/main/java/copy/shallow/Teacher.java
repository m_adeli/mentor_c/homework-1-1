package copy.shallow;

public class Teacher implements Cloneable {
    private String name;
    private Course course;
    private Book favouriteBook;

    public Teacher(String name, Course course, Book favouriteBook) {
        this.name = name;
        this.course = course;
        this.favouriteBook = favouriteBook;
    }

    public Book getFavouriteBook() {
        return favouriteBook;
    }

    public void setFavouriteBook(Book favouriteBook) {
        this.favouriteBook = favouriteBook;
    }

    // Shallow Copy
    @Override
    public Teacher clone() throws CloneNotSupportedException {
        return (Teacher) super.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
