package copy.shallow;

public class Book {
    private String name;
    private String subject;
    private String author;

    public Book(String name, String subject, String author) {
        this.name = name;
        this.subject = subject;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
