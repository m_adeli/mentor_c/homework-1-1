package copy.deep;

public class Book implements Cloneable {
    private String name;
    private String subject;
    private String author;

    public Book(String name, String subject, String author) {
        this.name = name;
        this.subject = subject;
        this.author = author;
    }

    @Override
    protected Book clone() throws CloneNotSupportedException {
        return (Book) super.clone();
    }

    @Override
    public String toString() {
        String result;

        result = this.getName() + " " +
                this.getSubject() + " " +
                this.getAuthor();
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
