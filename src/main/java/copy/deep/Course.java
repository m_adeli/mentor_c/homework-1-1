package copy.deep;

public class Course implements Cloneable{
    private String subject;


    public Course(String subject) {
        this.subject = subject;
    }

    @Override
    protected Course clone() throws CloneNotSupportedException {
        return (Course) super.clone();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
